package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

// Struct de datos que se manejaran dentro del servidor
type Datos struct {
	Materias map[string]map[string]float64
	Alumnos  map[string]map[string]float64
}

type PostData struct {
	Alumno       string
	Materia      string
	Calificacion float64
}

var d Datos

func agregar(datos PostData) []byte {
	// Verifica si la materia ya existe en el diccionario de datos
	if v, ok := d.Materias[datos.Materia]; ok {
		// En caso de existir la materia, se busca si existe el alumno.
		if _, ok2 := v[datos.Alumno]; ok2 {
			// Fracaso en la operación si ya existe el alumno
			return []byte(`{"code": "AlumnoExistente"}`)
		}
	}
	// Éxito en la operación, se registra en el map de Materias
	if d.Materias[datos.Materia] == nil {
		d.Materias[datos.Materia] = map[string]float64{}
	}
	d.Materias[datos.Materia][datos.Alumno] = datos.Calificacion
	// Éxito en la operación, se registra en el map de Alumnos
	if d.Alumnos[datos.Alumno] == nil {
		d.Alumnos[datos.Alumno] = map[string]float64{}
	}
	d.Alumnos[datos.Alumno][datos.Materia] = datos.Calificacion
	return []byte(`{"code": "ok"}`)
}

func mostrarTodos() []byte {
	if len(d.Alumnos) == 0 {
		return []byte(`{"code": "SinAlumnos"}`)
	}
	var reply string = "{\n"
	for alumno, matMap := range d.Alumnos {
		reply += "\t" + `"` + alumno + `" : {` + "\n"
		i := 0
		for mat, cal := range matMap {
			if i < len(matMap)-1 {
				reply += "\t\t" + `"` + mat + `" : ` + fmt.Sprint(cal) + ",\n"
			} else {
				reply += "\t\t" + `"` + mat + `" : ` + fmt.Sprint(cal) + "\n"
			}
			i++
		}
		reply += "\t}\n"
	}
	reply += "}"
	return []byte(reply)
}

func modificarAlumno(pData PostData) []byte {
	if mapAlumno, ok := d.Alumnos[pData.Alumno]; ok {
		if _, ok := mapAlumno[pData.Materia]; ok {
			d.Alumnos[pData.Alumno][pData.Materia] = pData.Calificacion
			d.Materias[pData.Materia][pData.Alumno] = pData.Calificacion
			return []byte(`{"code":"ok"}`)
		}
		return []byte(`{"code":"MateriaInexistente"}`)
	}
	return []byte(`{"code": "AlumnoInexistente"}`)
}

func mostrarAlumno(alumno string) []byte {
	if len(d.Alumnos) == 0 {
		return []byte(`{"code": "SinAlumnos"}`)
	}
	if mapAlumno, ok := d.Alumnos[alumno]; ok {
		var reply string = "{\n"
		reply += "\t" + `"` + alumno + `" : {` + "\n"
		i := 0
		for mat, cal := range mapAlumno {
			if i < len(mapAlumno)-1 {
				reply += "\t\t" + `"` + mat + `" : ` + fmt.Sprint(cal) + ",\n"
			} else {
				reply += "\t\t" + `"` + mat + `" : ` + fmt.Sprint(cal) + "\n"
			}
			i++
		}
		reply += "\t}\n"
		reply += "}"
		return []byte(reply)
	}
	return []byte(`{"code": "AlumnoInexistente"}`)
}

func eliminarAlumno(alumno string) []byte {
	if len(d.Alumnos) == 0 {
		return []byte(`{"code": "SinAlumnos"}`)
	}
	if mapAlumno, ok := d.Alumnos[alumno]; ok {
		for k, _ := range mapAlumno {
			delete(d.Materias[k], alumno)
		}
		delete(d.Alumnos, alumno)
		return []byte(`{"code":"ok"}`)
	}
	return []byte(`{"code": "AlumnoInexistente"}`)
}

func Crud(res http.ResponseWriter, req *http.Request) {
	// Toma el nombre del alumno
	alumno := strings.TrimPrefix(req.URL.Path, "/crud/")
	switch req.Method {
	case "POST":
		var bs PostData
		err := json.NewDecoder(req.Body).Decode(&bs)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		resJSON := agregar(bs)
		res.Header().Set(
			"Content-Type",
			"application/json",
		)
		res.Write(resJSON)
	case "GET":
		if alumno == "" {
			resJSON := mostrarTodos()
			res.Header().Set(
				"Content-Type",
				"application/json",
			)
			res.Write(resJSON)
		} else {
			resJSON := mostrarAlumno(alumno)
			res.Header().Set(
				"Content-Type",
				"application/json",
			)
			res.Write(resJSON)
		}
	case "DELETE":
		resJSON := eliminarAlumno(alumno)
		res.Header().Set(
			"Content-Type",
			"application/json",
		)
		res.Write(resJSON)
	case "PUT":
		var bs PostData
		err := json.NewDecoder(req.Body).Decode(&bs)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}
		resJSON := modificarAlumno(bs)
		res.Header().Set(
			"Content-Type",
			"application/json",
		)
		res.Write(resJSON)
	}
}

func main() {
	d = Datos{
		Materias: make(map[string]map[string]float64),
		Alumnos:  make(map[string]map[string]float64),
	}
	http.HandleFunc("/crud/", Crud)
	fmt.Println("Corriendo servirdor...")
	// Listener del servidor
	http.ListenAndServe(":8080", nil)
}
